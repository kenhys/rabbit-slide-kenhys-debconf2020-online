# Presentation memo

Hi, thank you for coming to this session.

I'll start the presentation "An experiment about personalized
front-end of bugs.debian.org.

## NOTE

This slide was already published at Rabbit Slide Show

So, you can also download this presentation.

## Personal profile

Let me just start by introducing myself.

I'm one of the Debian maintainers.  and a fan of a Trackpoint and Wasa
beef.  I'm also working for ClearCode.

## ClearCode Inc.

Usually, I'm developing and supporting software as an engineer.

There is a culture to respect free software, my colleague gave me a
feedback for this talk.

## Not talk about

In this session, this presentation is related to bugs.debian.org, but
I will not talk about improving bugs.debian.org itself and internal
details.

# Agenda

Here is the session agenda.

First, I'll explain why so curious about bugs.d.o
and show you the troublesome case.

Next, I'll explain how to solve this situation and demonstrate an
actual front-end.

# We use bugs.d.o

Here is the screenshot of bugs.d.o.

I guess you are familiar with it.

# bugs.debian.org

In bugs.d.o, we need to follow the rules.

1. Communicate each other by E-mail
2. Change the bug status by sending E-mail

# Why so curious about bugs.d.o?

One day, I've received deprecated notification about growl-for-linux
package.

growl-for-linux is a notification software.

One is related to libappindicator and the other is related to
dbus-glib.

# How did you fixed?

About libappindicator, just migrated to libayatana-indicator.

# How did you fixed? (again)

About, dbus-glib, I need to fix changed interfaces and migrated into
GDBus.

# growl-for-linux was fixed

Two bugs about growl-for-linux package was fixed, but I thought what
about other packages?

# What about other packages?

libappindicator and dbus-glib was tracked like this.

# libappindicator: deprecated in Debian;

Against libappindicator, there are many bugs.

# libdbus-glib-1-dev: is deprecated

Against dbus-glib, there are too many blocked bugs.

# How to track many blocked?

In these case, we can use UserTags to track bugs.

# Does it work with libappindicator?

For libappindicator, ayatana-appindicator is used as UserTag

# Does it work with libdbus-glib1-dev?

It should be tagged... but, UserTag is not available for dbus-glib.

# The troublesome case about bugs.d.o

Like this, there is a case that UserTag is not always used.  How to
solve this situation?

# Use udd.debian.org!

The answer is using udd.
UDD can track usertags and blocking bugs.

# In my experience, I need

So far, I've fixed or sent feedback to some bugs, I became to feel the
following ones are needed.

Tracking a specified bug
Finding a bug that no one working on
Sending control E-mail in required form
Finding affected bugs

# What I need for?

First topic is solved by using UDD.

But how about the rest issues?

# How to solve rest issues?

Surely, It is a good manner to fix a bug, but it is not easy.
It is also good to triage a bug, but it may be troublesome.

To know affected bugs are also important, but it is not so easy until
it hits you.

# Find affected bugs

In the past, there are some grave/critical bugs It had affected boot
sequence / logged in user session.  I want to avoid these bugs.

# What about existing solution?

I've searched existing useful services via Debian wiki.

Developer Horizon seems well, but not available.  Fabre also seems
well, but discontinued already.

I've also heard Debbugs Enhancement Suite.  But it is a bit different
what I want.

# What about existing solution?

As a result, Debian Popularity Content and Public UDD Mirror seems
useful.

# Starting personal project

So I've started a personal project.

The concept of this project is: "Make unstable life comfortable" It
seems that it is ridiculous, but important for me.

# Starting personal project

To achieve this concept, I need to do some tasks.

counting comment for each bugs, 
and adding separate mailto: link,
checking installed packages to alert affected bugs.

# How to realize concept

Furthermore, collecting E-mail archives, popcon data, installed
packages list and so on are also required.

# Collect bug information

To collect E-mail archives, we can use bugs-mirror with rsync.  E-mail
archive contains *.log and *.summary.

# Extract .log and .summary

There are many Perl modules to handle bugs.d.o related pieces of
stuff.

# How to parse .log

For example, Log and MIME module is used to parse .log file.

# How to parse .summary

Status module is used to parse .summary file.
You can access these bugs's metadata.

# Collect Popcon data and so on

As UDD provides public database access, 

So we can get popcon stats and investigate which bug affects unstable
or not.

# Collect installed packages information

As popularity-content command can collect installed binary packages
information.  It is useful to check recently used packages.

# Process collected data and make it accessible!

After collecting these information, updating bug information is
required regularly.

I've used debian-bugs-dist mailing list as a trigger to
update and imported it into database.

# Prepared server specs for experiment

For my experiment, I've prepared small VPS instance.

# System diagrams

Here is a simple diagram for explaining the system.

Web server is nginx, Application server is puma, and backend database is Groonga.

It access external services,
E-mail server, UDD for metadata, Salsa for authentication.

# Named my project as Fabre

As diagram indicates,
I've named this personal system as Fabre.

# Why Fabre?

Fabre is famous by the study of insects.
I've borrowed from discontinued similar project in decades ago

# Concept of Fabre (again)

The concept is this project is Make "unstable life" comfortable.

# Finding a bug that no one working on

Here is the partial screenshot of Fabre.
It shows each number of comments (orange background color means no one working on yet)

# Sending control E-mail easily

I've added each mailto: button to sending control mail

# Easy to view blocked bugs

I've aligned blocked bugs.

# Finding affected important bugs

I've added a dashboard to show fire icon. It indicates that there are affected bugs.

# How Fabre is good enough to work?

I'll show you some demo about Fabre.

# Current status of Fabre

That's all about Fabre demonstration.

In this system, tracked bugs are reached to eighty-nine thousand.
UDD tracks 90 thousand bugs, so it covers most of them.

Currently, bug information is updated every 1 hour, and affected bugs are also updated every 1 day.

Disk usage is about 1.5 GiB.

# Weak point of Fabre

There is a one weak point about Fabre.
out of memory is occurred sometimes.

# Conclusion

* There is plenty room of improvement to develop, so I'll go forward this experiment furthermore.
* In my experiment, I've found that mashup some data sources may improve user experience.

# Any questions?

Could you speak slowly if you ask, please?

# DEMO

I'll demonstrate some features of Fabre.

If you already sign-in with salsa account,
You can also sign in with Fabre.

You need explicitly allow to authorize with Fabre.

After sign in, you can access the dashboard.

In the dashboard, you can see a status.

As you can see, there are three affected bugs,
ten bookmarked bugs, and you installed thousand and six hundred packages.

At the bottom of the dashboard, you can check the existence of newer comments since you commented finally.

Let's see the affected bugs.
These information is based on your installed packages.

Let's see the bookmarked bugs.
In every bug detail page, bookmark button is shown, so you can track bug freely.

You can see recent update bugs, release critical bugs, and newcomer bugs.

Here is the bug detail page,

You can view original bug report via clicking link,
tweet about bugs,
and send control E-mail easily.

