Q. When Fabre is available?
A. I don't have concrete schedule about it.
   It is in very early stage of development, because I want to show proof of concept at DebConf.

Q. Where can I find the source code of Fabre?
A. I'll publish at Fabre code on salsa.d.o in the future.

Q. What can I help for Fabre?
A. I hope you will send cool logo for Fabre.

Q. Why doesn't send E-mail from system?
A. I think to send E-mail automatically is not appropriate because 
  the responsibility of sending E-mail should be owned by yourself.

Q. Why did you use Groonga?
A. Because I maintain Groonga package and I also want to support fulltext-search with it.
   Groonga is a column store database which supports fulltext-search.
